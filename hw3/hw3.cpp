#include <functional>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <vector>

#include <cstdint>
#include <cctype>


struct Point
{
    int64_t x;
    int64_t y;
};


enum class Position
{
    LEFT,
    BOTTOM,
    RIGHT,
    TOP
};


std::istream &operator >>(std::istream &in, Point &p)
{
    auto readCoordinate = [&in](int64_t &c) -> void {
        while (in.peek() != '-' && !isdigit(in.peek())) {
            in.ignore();
        }
        in >> c;
    };

    readCoordinate(p.x);
    readCoordinate(p.y);
    while (in.get() != ')') {}

    return in;
}


int64_t square(Point const &p1, Point const &p2, Point const &p3)
{
    return (p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x);
}


size_t binarySearch(
        size_t l, size_t r, size_t const mod,
        std::function<bool(size_t)> const &cmp)
{
    if (r < l)
        r += mod;

    while (l < r) {
        size_t m = l + (r - l) / 2;
        if (cmp(m % mod))
            l = m + 1;
        else
            r = m;
    }

    return l % mod;
}


Position findPosition(
        std::vector<Point> const &polygon,
        size_t const leftHigh, size_t const leftLow,
        size_t const rightLow, size_t const rightHigh,
        Point const &point)
{
    size_t const n = polygon.size();

    if (point.x < polygon[leftHigh].x)
        return Position::LEFT;

    if (point.x > polygon[rightLow].x)
        return Position::RIGHT;

    size_t const j = binarySearch(
            leftLow, rightLow, n,
            [&](size_t i) { return polygon[i].x <= point.x; });
    size_t const i = (j - 1 + n) % n;
    if (square(polygon[i], point, polygon[j]) > 0)
        return Position::BOTTOM;

    return Position::TOP;
}


std::pair<size_t, size_t> const findTangents(
        std::vector<Point> const &polygon,
        size_t const leftHigh, size_t const leftLow,
        size_t const rightLow, size_t const rightHigh,
        Point const &point)
{
    size_t const n = polygon.size();

    auto const s([&](size_t i) {
        return square(point, polygon[i], polygon[(i + 1) % n]);
    });

    Position const position = findPosition(polygon, leftHigh, leftLow,
                                           rightLow, rightHigh, point);
    switch (position) {
    case Position::LEFT:
        return std::make_pair(
                binarySearch(rightHigh, leftHigh, n,
                             [&](size_t i) { return s(i) >= 0; }),
                binarySearch(leftLow, rightLow, n,
                             [&](size_t i) { return s(i) < 0; }));
    case Position::RIGHT:
        return std::make_pair(
                binarySearch(rightHigh, leftHigh, n,
                             [&](size_t i) { return s(i) < 0; }),
                binarySearch(leftLow, rightLow, n,
                             [&](size_t i) { return s(i) >= 0; }));
    case Position::BOTTOM:
        return std::make_pair(
                binarySearch(leftLow, rightLow, n,
                             [&](size_t i) { return polygon[i].x < point.x
                                                    && s(i) >= 0; }),
                binarySearch(leftLow, rightLow, n,
                             [&](size_t i) { return polygon[i].x < point.x
                                                    || s(i) < 0; }));
    case Position::TOP:
        return std::make_pair(
                binarySearch(rightHigh, leftHigh, n,
                             [&](size_t i) { return polygon[i].x > point.x
                                                    && s(i) >= 0; }),
                binarySearch(rightHigh, leftHigh, n,
                             [&](size_t i) { return polygon[i].x > point.x
                                                    || s(i) < 0; }));
    }

    return std::make_pair(-1, -1);
}


std::tuple<size_t, size_t, size_t, size_t> const findBounds(
        std::vector<Point> const &polygon)
{
    auto const cmp1([](Point const &p, Point const &q) {
        return p.x < q.x || (p.x == q.x && p.y > q.y);
    });
    auto const cmp2([](Point const &p, Point const &q) {
        return p.x < q.x || (p.x == q.x && p.y < q.y);
    });

    size_t const leftHigh =
            std::min_element(polygon.begin(), polygon.end(), cmp1) -
            polygon.begin();
    size_t const leftLow =
            std::min_element(polygon.begin(), polygon.end(), cmp2) -
            polygon.begin();
    size_t const rightLow =
            std::max_element(polygon.begin(), polygon.end(), cmp1) -
            polygon.begin();
    size_t const rightHigh =
            std::max_element(polygon.begin(), polygon.end(), cmp2) -
            polygon.begin();

    return std::make_tuple(leftHigh, leftLow, rightLow, rightHigh);
}


int main()
{
    std::ios_base::sync_with_stdio(false);

    size_t n;
    std::cin >> n;
    std::vector<Point> polygon(n);
    std::copy_n(std::istream_iterator<Point>(std::cin), n, polygon.begin());

    size_t leftHigh, leftLow, rightLow, rightHigh;
    std::tie(leftHigh, leftLow, rightLow, rightHigh) = findBounds(polygon);

    size_t m;
    std::cin >> m;
    for (size_t i = 0; i < m; ++i) {
        Point p;
        std::cin >> p;
        auto const tangents(findTangents(
                polygon, leftHigh, leftLow, rightLow, rightHigh, p));
        std::cout << tangents.first << " " << tangents.second << "\n";
    }

    return 0;
}
