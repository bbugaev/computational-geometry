#! /usr/bin/env python2

import re
import sys
import functools as ft
import itertools as itt


def subtract(p, q):
    return (p[0] - q[0], p[1] - q[1])

def dot(p, q):
    return p[0] * q[0] + p[1] * q[1]

def sqr(p):
    return dot(p, p)

def cross(p, q):
    return p[0] * q[1] - p[1] * q[0]

def is_leftward(segment, point):
    p0, p1 = segment
    if p0[1] > p1[1]:
        p0, p1 = p1, p0
    return cross(subtract(p1, p0), subtract(point, p0)) > 0

def lies_on(segment, point):
    u = subtract(segment[1], segment[0])
    v = subtract(point, segment[0])
    return cross(u, v) == 0 and dot(u, v) >= 0 and sqr(u) >= sqr(v)

def make_pairs(it):
    it, tmp = itt.tee(it)
    return itt.izip(it, itt.islice(itt.cycle(tmp), 1, None))

def is_inside(polygon, point):
    polygon = list(make_pairs(map(ft.partial(subtract, q=point), polygon)))
    polygon_size = len(polygon)

    cnt = 0
    for i, s0 in enumerate(polygon):
        if lies_on(s0, (0, 0)):
            return True

        sgn = s0[0][1] * s0[1][1]
        if sgn < 0 and is_leftward(s0, (0, 0)):
            cnt += 1
        elif sgn == 0 and s0[0][1] != 0 and s0[1][0] > 0:
            j = (i + 1) % polygon_size
            while polygon[j][1][1] == 0:
                j = (j + 1) % polygon_size
            s1 = polygon[j]
            cnt += s0[0][1] * s1[1][1] < 0

    return cnt % 2 != 0

def read_int(src):
    return int(src.readline().strip())

def read_point(src):
    line = src.readline()
    return tuple(map(int, re.sub('[(),]', ' ', line).strip().split()))

def read_points(src, point_count):
    return [read_point(src) for _ in xrange(point_count)]

def main(src, dst):
    polygon_size = read_int(src)
    polygon = read_points(src, polygon_size)
    query_count = read_int(src)
    results = map(ft.partial(is_inside, polygon),
                  read_points(src, query_count))
    dst.write('\n'.join('yes' if ok else 'no' for ok in results) + '\n')


if __name__ == '__main__':
    main(sys.stdin, sys.stdout)
