#! /usr/bin/env python

import hw1
import glob
import StringIO
import unittest
import os.path as path
import functools as ft


class Tester(unittest.TestCase):
    pass

def create_test(src_name, ans_name):
    def run_test(self):
        with open(src_name, 'r') as src:
            res = StringIO.StringIO()
            hw1.main(src, res)
        with open(ans_name, 'r') as ans:
            ans_list = [s.strip() for s in ans]
            res_list = res.getvalue().strip().split()
            self.assertTrue(cmp(ans_list, res_list) == 0,
                            'Incorrect results')
    return run_test

TEST_FOLDER = 'tests'
IN_EXTENSION = '.in'
OUT_EXTENSION = '.out'

in_files = glob.glob(path.join(TEST_FOLDER, '*' + IN_EXTENSION))
out_files = glob.glob(path.join(TEST_FOLDER, '*' + OUT_EXTENSION))
in_files = [s.replace(IN_EXTENSION, '') for s in in_files]
out_files = [s.replace(OUT_EXTENSION, '') for s in out_files]
files = set(in_files).intersection(out_files)
for f in files:
    setattr(
        Tester,
        'test_' + path.split(f)[1],
         create_test(src_name=f + IN_EXTENSION, ans_name=f + OUT_EXTENSION))


if __name__ == '__main__':
    unittest.main()
