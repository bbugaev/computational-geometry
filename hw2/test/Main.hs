module Main (
    main
) where

import Test.Framework
import TriangulationTest


main :: IO ()
main = defaultMain $ triangulationTests
