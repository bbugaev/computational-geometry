module TriangulationTest (
    triangulationTests
) where

import Test.Framework
import Test.Framework.Providers.HUnit
import Test.HUnit hiding (Test)
import Data.Vector (fromList, (!))
import Point
import Triangulation


polygons :: [[Point Int64]]
polygons =
    [[Point 1 1, Point 5 2, Point 3 7],
     [Point (negate 3) (negate 2), Point 1 (negate 2), Point 3 2, Point 1 2],
     [Point 1 1, Point 3 0, Point 5 1, Point 4 3, Point 2 3],
     [Point 0 0, Point 5 0, Point 4 4, Point 3 3, Point 2 2, Point 1 1]]

namedPolygons :: [([Point Int64], String)]
namedPolygons = map (\(i, p) -> (p, "Polygon " ++ show i)) (zip [1..] polygons)


createTestGroup :: String -> (([Point Int64], String) -> Test) -> Test
createTestGroup name createTest = testGroup name $ map createTest namedPolygons


triangleCount :: Test
triangleCount = createTestGroup "Triangle Count" createTest
  where
    createTest (p, n) = testCase n ((length p) - 2 @=? length (triangulate p))

ccwTriangle :: Test
ccwTriangle = createTestGroup "CCW Triangle" createTest
  where
    createTest (p, n) = testGroup n $ map testTriangle (triangulate p)
      where
        testTriangle t = testCase (show t) $ assertBool "CW" (ccwIndices t)
        ccwIndices (i, j, k) = (ccw (ps ! i) (ps ! j) (ps ! k))
        ps = fromList p

squareSum :: Test
squareSum = createTestGroup "Squares" createTest
  where
    calc2S p = sum $ map (\(Point x y, Point x' y') -> x * y' - x' * y)
                         (zip p (tail $ cycle p))
    createTest (p, n) = testCase n (sum (map calc2S triangles) @=? calc2S p)
      where
        triangles = map (\(i, j, k) -> [ps ! i, ps ! j, ps ! k])
                        (triangulate p)
        ps = fromList p


triangulationTests :: [Test]
triangulationTests = [triangleCount, ccwTriangle, squareSum]
