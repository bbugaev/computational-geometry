module Triangulation (
    triangulate,
    Point,
    Int64
) where

import Data.Int
import Point
import Utils


data Chain = Low | High deriving Show

instance Eq Chain where
    Low == Low = True
    High == High = True
    _ == _ = False


isEar :: (Num a, Ord a) => Chain -> Point a -> Point a -> Point a -> Bool
isEar High = ccw
isEar Low = cw

triangle :: Chain -> Int -> Int -> Int -> (Int, Int, Int)
triangle High i j k = minimum [(i, j, k), (j, k, i), (k, i, j)]
triangle Low i j k = triangle High i k j

preparePoints :: (Ord a) => [Point a] -> [(Point a, Int, Chain)]
preparePoints pts = mergeBy (\(p, _, _) (q, _, _) -> p < q) low (reverse high)
  where
    epts = zip pts [0..]
    maxPt = maximum epts
    (low', high') = splitBefore maxPt epts
    low = (\(a, b) -> zip3 a b (repeat Low)) (unzip low')
    high = (\(a, b) -> zip3 a b (repeat High)) (unzip high')

triangulate :: (Num a, Ord a) => [Point a] -> [(Int, Int, Int)]
triangulate points = let (p1 : p0 : ps) = preparePoints points in
                     impl ps [p0, p1] []
  where
    impl [] _ triangles = triangles
    impl points@(p@(_, _, chain) : ps)
         stack@(q@(_, _, chain') : _)
         triangles
        | chain == chain' = eqChains points stack triangles
        | otherwise = impl ps [p, q] (neqChains p stack triangles)
    impl _ _ _ = undefined

    eqChains (point@(p, i, chain) : points)
             stack@((q1, j, _) : q0t@(q0, k, _) : stackTail)
             triangles
        | isEar chain p q1 q0 = eqChains (point : points)
                                         (q0t : stackTail)
                                         (triangle chain i j k : triangles)
        | otherwise = impl points (point : stack) triangles
    eqChains (point : points)
             stack@[_]
             triangles
        = impl points (point : stack) triangles
    eqChains _ _ triangles = triangles

    neqChains point@(_, i, _)
              ((_, j, chain) : stackTail@((_, k, _) : _))
              triangles
        = neqChains point stackTail (triangle chain i j k : triangles)
    neqChains _ _ triangles = triangles
