module Point (
    Point (Point),
    ccw,
    cw
) where

import Data.Char (isSpace)


data Point a = Point a a

instance (Show a) => Show (Point a) where
    show (Point x y) = show (x, y)

instance (Read a) => Read (Point a) where
    readsPrec _ = readParen False readPoint
      where
        readPoint s = concatMap helper (reads s)
          where
            helper (x, s') = let isSkipped = \c -> (isSpace c) || (c == ',') in
                             map (\(y, s'') -> (Point x y, s''))
                                 (reads $ dropWhile isSkipped s')

instance (Eq a) => Eq (Point a) where
    (==) (Point x y) (Point x' y') = x == x' && y == y'

instance (Ord a) => Ord (Point a) where
    (<=) (Point x y) (Point x' y') = x < x' || (x == x' && y <= y')

minus :: (Num a) => Point a -> Point a -> Point a
(Point x y) `minus` (Point x' y') = Point (x - x') (y - y')

cross :: (Num a) => Point a -> Point a -> a
(Point x y) `cross` (Point x' y') = x * y' - x' * y

ccw :: (Num a, Ord a) => Point a -> Point a -> Point a -> Bool
ccw p q1 q0 = ((q1 `minus` p) `cross` (q0 `minus` p)) > 0

cw :: (Num a, Ord a) => Point a -> Point a -> Point a -> Bool
cw p q1 q0 = ((q1 `minus` p) `cross` (q0 `minus` p)) < 0
