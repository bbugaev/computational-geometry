module Main where

import Control.Monad
import Triangulation


readPolygon :: IO [Point Int64]
readPolygon = do
    pointCount <- readLn :: IO Int
    forM [1.. pointCount] $ const (readLn :: IO (Point Int64))

main :: IO ()
main = do
    polygon <- readPolygon
    mapM_ print $ triangulate polygon
