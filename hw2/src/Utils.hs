module Utils where


splitBefore :: (Eq a) => a -> [a] -> ([a], [a])
splitBefore x xs = splitBeforeImpl xs []
  where
    splitBeforeImpl [] fsts = (reverse fsts, [])
    splitBeforeImpl (y : ys) fsts | y == x = (reverse fsts, y : ys)
                                  | otherwise = splitBeforeImpl ys (y : fsts)

mergeBy :: (a -> a -> Bool) -> [a] -> [a] -> [a]
mergeBy _ [] xs = xs
mergeBy _ xs [] = xs
mergeBy cmp ls@(x : xs) rs@(y : ys) | cmp x y = x : mergeBy cmp xs rs
                                    | otherwise = y : mergeBy cmp ls ys
